document.addEventListener('DOMContentLoaded', () => {
    const loginForm = document.getElementById('loginForm')

    loginForm.addEventListener('submit', function (e) {
        e.preventDefault()
        const username = document.getElementById('username').value
        const password = document.getElementById('password').value

        fetch('/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ username, password }),
        })
            .then((response) => {
                if (!response.ok) {
                    throw new Error('Network response was not ok')
                }
                return response.json()
            })
            .then((data) => {
                if (data.redirectUrl) {
                    window.location.href = data.redirectUrl
                } else {
                    document.getElementById('errorMessage').textContent =
                        data.message
                }
            })
            .catch((error) => {
                console.error('Hiba történt:', error)
                document.getElementById('errorMessage').textContent =
                    'Hibás felhasználónév vagy jelszó!'
            })
    })
})
