document.addEventListener('DOMContentLoaded', () => {
    const form = document.querySelector('form')
    const submit = document.querySelector('.submit')
    const urlParams = new URLSearchParams(window.location.search)
    const personId = urlParams.get('id')

    // fetchData hívása a DOMContentLoaded után
    fetchData(personId)

    submit.addEventListener('click', () => {
        const name = form[0].value
        const email = form[1].value
        const phone = form[2].value
        const address = form[3].value

        fetch(`/person/${personId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                name: name,
                email: email,
                phone: phone,
                address: address,
            }),
        })
            .then((response) => response.text())
            .then((data) => {
                alert(data)
                window.location.href = 'index.html'
            })
            .catch((error) => {
                console.error('Hiba történt:', error)
            })
    })
})

async function fetchData(id) {
    await fetch(`/person/${id}`)
        .then((response) => {
            if (!response.ok) {
                throw new Error('Network response was not ok')
            }
            return response.json()
        })
        .then((data) => {
            console.log(data)

            document.getElementById('name').value = data[0].name
            document.getElementById('email').value = data[0].email
            document.getElementById('phone').value = data[0].phone
            document.getElementById('address').value = data[0].address
        })
        .catch((error) => {
            console.error('Hiba történt:', error)
        })
}
