const express = require('express')
const mysql = require('mysql2')
const path = require('path')
const cookieParser = require('cookie-parser') // cookie-parser modul importálása

const app = express()
const port = 3000

// Express middleware beállítása JSON adatok fogadásához
app.use(express.json())
app.use(express.static(path.join(__dirname, '.')))
app.use(cookieParser()) // Cookie-parser beállítása

// MySQL adatbázis kapcsolat beállítása
const db = mysql.createConnection({
    port: 3306,
    host: 'localhost',
    user: 'root', // MySQL felhasználónév
    password: 'kinga', // MySQL jelszó
    database: 'persons', // Adatbázis neve
})

db.connect((err) => {
    if (err) {
        console.error('Hiba történt a csatlakozás során:', err)
        throw err
    }
    console.log('Sikeresen csatlakozva az adatbázishoz')
})

// Bejelentkezés kezelése
app.post('/login', (req, res) => {
    const { username, password } = req.body
    const query = 'SELECT * FROM users WHERE username = ? AND password = ?'
    db.query(query, [username, password], (err, result) => {
        if (err) {
            console.error('Hiba történt a bejelentkezés során:', err)
            return res.status(500).json({ message: 'Internal server error' })
        }
        if (result.length > 0) {
            const user = result[0]
            if (user.isAdmin) {
                // Beállítjuk az admin jogosultságú cookie-t
                res.cookie('isAdmin', true)
            } else {
                // Ha nem admin, töröljük az isAdmin sütit
                res.clearCookie('isAdmin')
            }
            // Beállítjuk a felhasználónevet a cookie-ban
            res.cookie('username', username)
            res.json({ username: username, redirectUrl: '/about.html' })
        } else {
            res.status(401).json({ message: 'Invalid username or password' })
        }
    })
})

// Middleware a bejelentkezés ellenőrzésére
const checkLoggedIn = (req, res, next) => {
    // Ha a felhasználónak van felhasználóneve a cookie-ban, akkor engedélyezzük az adott útvonalhoz való hozzáférést
    if (req.cookies.username) {
        next()
    } else {
        // Ha nincs bejelentkezett felhasználó, akkor átirányítjuk a login oldalra
        res.redirect('/login.html')
    }
}

// Middleware az admin jogosultság ellenőrzésére
const checkAdmin = (req, res, next) => {
    // Ellenőrizzük az admin jogosultságot
    if (req.cookies.isAdmin) {
        // Ha admin, akkor engedélyezzük az adott útvonalhoz való hozzáférést
        next()
    } else {
        // Ha nem admin, akkor átirányítjuk egy hozzáférés megtagadva oldalra
        res.status(403).send('Access Denied')
    }
}

app.use(checkLoggedIn)

// app.get('/login', (req, res) => {
//     res.redirect('/login.html');
// });
app.get('/login', (req, res) => {
    if (!req.cookies.username) {
        res.sendFile(path.join(__dirname, '/login.html'))
    } else {
        // Ha van bejelentkezett felhasználó, akkor irányítsa át az index oldalra
        res.redirect('/about.html')
    }
})

app.get('/about.html', (req, res) => {
    // Ellenőrizd, hogy a felhasználó be van-e jelentkezve
    if (req.cookies.username) {
        // Ha be van jelentkezve, akkor szolgáltasd ki az index oldalt
        res.sendFile(path.join(__dirname, '/about.html'))
    } else {
        // Ha nincs bejelentkezve, akkor irányítsd át a felhasználót a bejelentkezési oldalra
        res.redirect('/login.html')
    }
})

// Az index oldal csak bejelentkezés után érhető el
app.get('/', (req, res) => {
    // Ellenőrizd, hogy a felhasználó be van-e jelentkezve
    if (req.cookies.username) {
        // Ha be van jelentkezve, akkor szolgáltasd ki az index oldalt
        res.sendFile(path.join(__dirname, '/about.html'))
    } else {
        // Ha nincs bejelentkezve, akkor irányítsd át a felhasználót a bejelentkezési oldalra
        res.redirect('/login.html')
    }
})

// Szerver oldali kód a felhasználó nevének lekérésére
app.get('/getUsername', (req, res) => {
    const username = req.cookies.username
    res.json({ username: username })
})

// Az admin oldal csak admin jogosultsággal rendelkező felhasználók számára érhető el
app.get('/admin', checkLoggedIn, checkAdmin, (req, res) => {
    res.sendFile(path.join(__dirname, 'admin.html'))
})

// Kijelentkezés kezelése
app.post('/logout', (req, res) => {
    res.clearCookie('username') // Töröljük a felhasználónév cookie-t
    res.clearCookie('isAdmin') // Töröljük az admin cookie-t
    res.redirect('/login.html')
})

app.get('/search', (req, res) => {
    const searchTerm = req.query.term.toLowerCase()
    const sql =
        'SELECT * FROM persons WHERE LOWER(name) LIKE ? OR LOWER(email) LIKE ? OR LOWER(phone) LIKE ? OR LOWER(address) LIKE ?'
    const params = [
        `%${searchTerm}%`,
        `%${searchTerm}%`,
        `%${searchTerm}%`,
        `%${searchTerm}%`,
    ]
    db.query(sql, params, (err, result) => {
        if (err) {
            console.error('Hiba történt a keresés során:', err)
            res.status(500).json({ error: 'Hiba történt a keresés során' })
        } else {
            res.send(JSON.stringify(result))
        }
    })
})

//Elérési pont az adatok hozzáadásához
app.post('/person', (req, res) => {
    const { name, email, phone, address } = req.body
    const sql =
        'INSERT INTO persons (name, email, phone, address) VALUES (?, ?, ?, ?)'
    db.query(sql, [name, email, phone, address], (err, result) => {
        if (err) {
            console.error('Hiba történt az adatok hozzáadása közben:', err)
            res.status(500).send('Hiba történt az adatok hozzáadása közben')
        } else {
            console.log('Adatok sikeresen hozzáadva')
            res.send('Adatok sikeresen hozzáadva')
        }
    })
})

// Elérési pont az adatok lekérdezéséhez
app.get('/getData', (req, res) => {
    const sql = 'SELECT * FROM persons'
    db.query(sql, (err, result) => {
        if (err) {
            console.error('Hiba történt az adatok lekérése közben:', err)
            res.status(500).json({
                error: 'Hiba történt az adatok lekérése közben',
            })
        } else {
            res.send(JSON.stringify(result))
        }
    })
})

app.get('/person/:id', (req, res) => {
    const id = req.params.id
    const sql = 'SELECT * FROM persons WHERE idpersons = ?'
    db.query(sql, id, (err, result) => {
        if (err) {
            console.error('Hiba történt az adatok lekérése közben:', err)
            res.status(500).json({
                error: 'Hiba történt az adatok lekérése közben',
            })
        } else {
            res.send(JSON.stringify(result))
        }
    })
})

// Elérési pont az adatok törléséhez
app.delete('/delete/:id', (req, res) => {
    const id = req.params.id
    const sql = 'DELETE FROM persons WHERE idpersons = ?'
    db.query(sql, id, (err, result) => {
        if (err) {
            console.error('Hiba történt az adatok törlése közben:', err)
            res.status(500).send('Hiba történt az adatok törlése közben')
        } else {
            console.log('Adatok sikeresen törölve')
            res.send('Adatok sikeresen törölve')
        }
    })
})

// Elérési pont az adatok frissítéséhez
app.put('/person/:id', (req, res) => {
    const id = req.params.id
    const { name, email, phone, address } = req.body
    const sql =
        'UPDATE persons SET name = ?, email = ?, phone = ?, address = ? WHERE idpersons = ?'
    db.query(sql, [name, email, phone, address, id], (err, result) => {
        if (err) {
            console.error('Hiba történt az adatok frissítése közben:', err)
            res.status(500).send('Hiba történt az adatok frissítése közben')
        } else {
            console.log('Adatok sikeresen frissítve')
            res.send('Adatok sikeresen frissítve')
        }
    })
})

// Általános hibakezelés
app.use((err, req, res, next) => {
    console.error(err.stack)
    res.status(500).send('Valami hiba történt a szerveren')
})

app.listen(port, () => {
    console.log(`A szerver fut a http://localhost:${port} címen`)
})
