document.addEventListener('DOMContentLoaded', async () => {
    const form = document.querySelector('form')
    const submit = document.querySelector('.submit')
    const addPersonButton = document.getElementById('addPerson')

    await fetchData()
    loadUsername()

    async function fetchData() {
        const tbody = document.querySelector('table tbody')
        try {
            const response = await fetch('/getData')
            if (!response.ok) {
                throw new Error('Network response was not ok')
            }
            const data = await response.json()
            const isAdmin = document.cookie
                .split(';')
                .some((cookie) => cookie.trim().startsWith('isAdmin='))
            tbody.innerHTML = ''
            data.forEach((item) => {
                tbody.innerHTML += `
                    <tr data-id="${item.idpersons}">
                        <td>${item.name}</td>
                        <td>${item.email}</td>
                        <td>${item.phone}</td>
                        <td>${item.address}</td>
                        ${
                            isAdmin
                                ? `
                            <td>
                                <a href="./editPerson.html?id=${item.idpersons}"><button class="updateButton" data-id="${item.id}">Edit</button></a>
                            </td>
                            <td>
                                <button class="deleteButton" data-id="${item.idpersons}">Delete</button>
                            </td>
                        `
                                : ''
                        }
                    </tr>
                `
            })
            document.querySelectorAll('.deleteButton').forEach((button) => {
                button.addEventListener('click', function () {
                    const idToDelete = this.getAttribute('data-id')
                    deleteRow(idToDelete)
                })
            })
        } catch (error) {
            console.error('Hiba történt:', error)
        }
    }

    function deleteRow(id) {
        console.log('Delete row with ID:', id)
        // Delete request ide
        if (confirm(`Biztosan törölni szeretné a személyt?`)) {
            fetch(`/delete/${id}`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                },
            })
                .then((response) => {
                    if (!response.ok) {
                        throw new Error('Network response was not ok')
                    }
                    return response.text()
                })
                .then((data) => {
                    alert(data)
                    location.reload()
                })
                .catch((error) => {
                    console.error(
                        'Hiba történt az adatok törlése közben:',
                        error
                    )
                    alert(
                        'Hiba történt az adatok törlése közben. Kérjük, próbálkozzon újra később.'
                    )
                })
        } else {
            console.log('A törlés megszakítva.')
        }
    }

    // Function to load username
    async function loadUsername() {
        try {
            const response = await fetch('/getUsername')
            if (!response.ok) {
                throw new Error('Network response was not ok')
            }
            const data = await response.json()
            const userSpan = document.createElement('span')
            userSpan.textContent = data.username
            document.querySelector('.user-info').appendChild(userSpan)
        } catch (error) {
            console.error(
                'Hiba történt a felhasználó nevének betöltésekor:',
                error
            )
        }
    }

    const logoutLink = document.getElementById('logout')
    logoutLink.addEventListener('click', async (event) => {
        event.preventDefault() // Megakadályozza az alapértelmezett link működését (az oldal frissülését)
        try {
            const logoutResponse = await fetch('/logout', {
                method: 'POST', // A kijelentkezéshez POST kérést küldünk
            })
            if (logoutResponse.ok) {
                // Ha a kijelentkezés sikeres volt, az oldalt újratöltjük, vagy más kezelést végezhetünk
                window.location.href = 'login.html'
            } else {
                console.error(
                    'Hiba történt a kijelentkezés során:',
                    logoutResponse.statusText
                )
            }
        } catch (error) {
            console.error('Hiba történt a kijelentkezés során:', error)
        }
    })

    async function search(query) {
        const tbody = document.querySelector('table tbody')
        try {
            const response = await fetch(`/search?term=${query}`)
            if (!response.ok) {
                throw new Error('Network response was not ok')
            }
            const data = await response.json()
            tbody.innerHTML = '' // Töröljük az előző adatokat a táblázatból
            if (data.length === 0) {
                tbody.innerHTML =
                    '<tr><td colspan="4">No results found</td></tr>'
            } else {
                const isAdmin = document.cookie
                    .split(';')
                    .some((cookie) => cookie.trim().startsWith('isAdmin='))
                data.forEach((item) => {
                    tbody.innerHTML += `
                        <tr data-id="${item.idpersons}">
                            <td>${item.name}</td>
                            <td>${item.email}</td>
                            <td>${item.phone}</td>
                            <td>${item.address}</td>
                            ${
                                isAdmin
                                    ? `
                                <td>
                                    <a href="./editPerson.html?id=${item.idpersons}"><button class="updateButton" data-id="${item.id}">Edit</button></a>
                                </td>
                                <td>
                                    <button class="deleteButton" data-id="${item.idpersons}">Delete</button>
                                </td>
                            `
                                    : ''
                            }
                        </tr>
                    `
                })
                document.querySelectorAll('.deleteButton').forEach((button) => {
                    button.addEventListener('click', function () {
                        const idToDelete = this.getAttribute('data-id')
                        deleteRow(idToDelete)
                    })
                })
            }
        } catch (error) {
            console.error('Hiba történt a keresés során:', error)
        }
    }

    searchButton.addEventListener('click', async () => {
        const searchTerm = searchInput.value.toLowerCase()
        await search(searchTerm)
        searchInput.value = ''
    })

    searchInput.addEventListener('keypress', async (e) => {
        if (e.key === 'Enter') {
            const searchTerm = searchInput.value.toLowerCase()
            await search(searchTerm)
        }
    })

    listAllButton.addEventListener('click', async () => {
        await fetchData()
    })
})

window.addEventListener('scroll', function () {
    var footer = document.querySelector('footer')
    var scrollPosition = window.innerHeight + window.scrollY
    var bodyHeight = document.body.offsetHeight

    if (scrollPosition >= bodyHeight) {
        footer.style.position = 'sticky'
        footer.style.bottom = '0'
        footer.style.top = 'auto'
        footer.style.zIndex = '1000'
    } else {
        footer.style.position = 'static'
    }
})
