document.addEventListener('DOMContentLoaded', () => {
    const form = document.querySelector('form')
    const submit = document.querySelector('.submit')
    const addPersonButton = document.getElementById('addPerson')

    submit.addEventListener('click', () => {
        const name = form[0].value
        const email = form[1].value
        const phone = form[2].value
        const address = form[3].value

        fetch('/person', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                name: name,
                email: email,
                phone: phone,
                address: address,
            }),
        })
            .then((response) => response.text())
            .then((data) => {
                alert(data)
                window.location.href = 'index.html'
            })
            .catch((error) => {
                console.error('Hiba történt:', error)
            })
    })
})
